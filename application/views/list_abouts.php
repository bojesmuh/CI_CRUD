<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Data Abouts
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Abouts</h3><br>
              <a href="<?= base_url('index.php/abouts/add'); ?>" class="btn btn-primary"><i class="fa fa-plus"></i> ADD </a>
              <button type="" class="btn btn-default" onclick="reload_table()"><i class="fa fa-refresh"></i> Reload</button>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="table" class="table table-bordered table-hover">
                <thead>
                    <tr>
                        <td>No</td>
                        <td>Title</td>
                        <td>Contents</td>
                        <td>Action</td>
                    </tr>
                </thead>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

          
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

 
<!-- DataTables -->
<script src="<?= site_url(''); ?>/../assets/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?= site_url(''); ?>/../assets/plugins/datatables/dataTables.bootstrap.min.js"></script>

<script type="text/javascript">

    var save_method; //for save method string
    var table;
    // $(document).ready(function() {
      table = $('#table').DataTable({ 
        
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        
        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "<?php echo site_url('abouts/ajax_list'); ?>",
            "type": "POST"
        },

        //Set column definition initialisation properties.
        "columnDefs": [
        { 
          "targets": [ -1 ], //last column
          "orderable": false, //set not orderable
        },
        ],

      });

    $("input").change(function(){
        $(this).parent().parent().removeClass('has-error');
        $(this).next().empty();
    });

    function reload_table()
    {
      table.ajax.reload(null,false); //reload datatable ajax 
    }
    

  </script>