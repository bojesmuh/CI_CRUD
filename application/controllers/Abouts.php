<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Abouts extends CI_Controller {

    
    public function __construct()
    {
        parent::__construct();
        //Do your magic here
        $this->load->model('M_abouts','abouts');
        
    }
    

    public function index()
    {
        $data = array(
            "title" => "Abouts"
        );

        $this->load->view('header', $data, FALSE);
        $this->load->view('sidebar', $data, FALSE);
        $this->load->view('list_abouts', $data, FALSE);
        $this->load->view('footer', $data, FALSE);
    }

    public function add()
    {
        $data = array(
            "title" => "Abouts"
        );

        $this->load->view('header', $data, FALSE);
        $this->load->view('sidebar', $data, FALSE);
        $this->load->view('c_abouts', $data, FALSE);
        $this->load->view('footer', $data, FALSE);
    }

    public function ajax_list()
	{
		$list = $this->abouts->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $r) {
			$no++;
			$row = array();
			$row[] = $no;
			$row[] = $r->title_abouts;
			$row[] = $r->content_abouts;
			
			//add html for action
			$row[] = "
			<a class=\"btn btn-sm btn-primary\" href='".base_url('abouts/update')."/$r->id_abouts' title=\"Edit\"><i class=\"glyphicon glyphicon-pencil\"></i> Edit</a>
			 
              <a href='".base_url('abouts/delete')."/$r->id_abouts' onclick=\"javasciprt: return confirm('Anda Yakin ?')\" class=\"btn btn-sm btn-danger\"><i class=\"fa fa-times\"></i> Delete</a>";
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->abouts->count_all(), // ambil data dari model dengan nama model M_abouts.php
						"recordsFiltered" => $this->abouts->count_filtered(), // ambil data dari model dengan nama model M_abouts.php
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

}

/* End of file Controllername.php */
