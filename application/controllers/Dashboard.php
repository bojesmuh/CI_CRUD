<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

    public function index()
    {
        $data = array(
            "title" => "Dashboard"
        );

        $this->load->view('header', $data, FALSE);
        $this->load->view('sidebar', $data, FALSE);
        $this->load->view('c_dashboard', $data, FALSE);
        $this->load->view('footer', $data, FALSE);
    }

}

/* End of file Controllername.php */
